<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mdbase extends CI_Model {

  function login($email, $password){
    $this->db->where('email_usr', $email);
    return $this->db->get('user')->row();
    $this->db->where('password_akun', $password);
    return $this->db->get('akun')->row();
  }

  function insert($dataUser, $password){
    $this->db->insert('user',$dataUser);

    $this->db->select('user.*, akun.*');
    $this->db->from('user');
    $this->db->join('akun','user.id_usr = akun.id_usr');

    $dataAkun = array('password_akun' => $password );
    $this->db->insert('akun',$dataAkun);
  }

  function getDataUser($email){
    $this->db->select('*');
    $this->db->where('email_usr', $email);
    $data = $query = $this->db->get('user');
    return $query->result();
  }

  function updateDataUser($email, $data){
    $this->db->where('email_usr', $email);
    $this->db->update('user', $data);
  }

  function insert_file($filename, $title){
      $data = array(
          'filename'      => $filename,
          'title'         => $title
      );
      $this->db->insert('files', $data);
      return $this->db->insert_id();
  }

  function getDataItem($kd){
    $this->db->select('item.*, user.kode_item');
    $this->db->from('item');
    $this->db->join('user','item.kode_item = user.kode_item');
    $this->db->where('kode_item', $kd);
    $query = $this->db->get()->row;
  }

}
