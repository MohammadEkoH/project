<?php
defined('BASEPATH') OR exit('No direct script access allowed');

  class ModelAdmin extends CI_Model {
    function __construct(){
     parent::__construct();
   }
   function insert($table = '', $data = ''){
     $this->db->insert($table, $data);
   }
   function get_all($table){
     $this->db->from($table);
     return $this->db->get();
   }
   function get_where($table = null, $where = null){
     $this->db->from($table);
     $this->db->where($where);

     return $this->db->get();
   }

   public function find($kode_item){
     //Query mencari record berdasarkan ID-nya
     $hasil = $this->db->where('kode_item', $kode_item)
               ->limit(1)
               ->get('item');
     if($hasil->num_rows() > 0){
       return $hasil->row();
     } else {
       return array();
     }
   }

   public function update($kode_item, $items){
     $this->db->where('kode_item', $kode_item)
          ->update('item', $items);
   }

   public function delete($kode_item){
     $this->db->where('kode_item', $kode_item)
              ->delete('item');
   }

   //Json
   function get_data($kode_item) {
      $query = $this->db->select('*')
                        ->from('item')
                        ->where('kode_item', $kode_item)
                        ->get();

      if ($query->num_rows() > 0) {
          foreach ($query->result() as $data) {
              $hasil[] = $data;
          }
          return $hasil;
      }
    }
}
