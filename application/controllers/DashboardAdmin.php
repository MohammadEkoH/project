<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashboardAdmin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('template');
		$this->load->library(array('template', 'form_validation'));
	}

	public function adminHome($kode_item=20){
		// $kode_item = $this->uri->segment(3);
    $data['data'] = $this->ModelAdmin->get_all('item');
		$items = $this->ModelAdmin->get_where('item', array('kode_item' => $kode_item))->result();
		foreach ($items as $key) {
			$data['name_item'] = $key->name_item;
			$data['harga_item'] = $key->harga_item;
			$data['stok_item'] = $key->stok_item;
			$data['status_item'] = $key->status_item;
			$data['gambar_item'] = $key->gambar_item;
			$data['deskripsi_item'] = $key->deskripsi_item;
		}
    $this->template->admin('admin/manage_item', $data);
  }

  public function add_item(){
    if ($this->input->post('submit', TRUE) == 'submit'){
      //validasi
      $this->form_validation->set_rules('nama', 'Nama Item', 'required|min_length[4]');
      $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
			$this->form_validation->set_rules('stok', 'Stok Item', 'required|numeric');
      $this->form_validation->set_rules('status', 'Status', 'required|numeric');
      $this->form_validation->set_rules('desk', 'Deskripsi', 'required|min_length[4]');

			$name = $this->input->post('nama', TRUE);

      if ($this->form_validation->run() == TRUE){
        //upload gambar
        $config['upload_path'] = './assets/images/';
        $config['allowed_types'] ='jpg|png|jpeg';
        $config['max_size'] ='2048';
        $config['file_name'] = 'foto'.$name;

        $this->load->library('upload', $config);

        if($this->upload->do_upload('foto')){
          $gbr = $this->upload->data();
          //proses Insert
          $items = array( 
            'name_item' => $name,
            'harga_item' => $this->input->post('harga', TRUE),
						'stok_item' => $this->input->post('stok', TRUE),
            'status_item' => $this->input->post('status', TRUE),
            'gambar_item' => $gbr['file_name'],
            'deskripsi_item' => $this->input->post('desk', TRUE),
          );
          $this->ModelAdmin->insert('item', $items);
					$this->session->set_flashdata('success', 'Berhasil menambahkan item');
        } else {
          $this->session->set_flashdata('alert', 'Anda Belum Memilih Foto');
        }
      }

			redirect('DashboardAdmin/adminHome');
    }

    // $data['header'] = "Add New Item";

    // $this->template->admin('admin/item_form', $data);

  }

  public function edit_item($kode_item){
		$item = $this->ModelAdmin->get_where('item', array('kode_item' => $kode_item));
		foreach ($item->result() as $key) {
        $data['name_item'] = $key->name_item;
        $data['harga_item'] = $key->harga_item;
				$data['stok_item'] = $key->stok_item;
        $data['status_item'] = $key->status_item;
        $data['gambar_item'] = $key->gambar_item;
        $data['deskripsi_item'] = $key->deskripsi_item;
    }

		if ($this->input->post('submit', TRUE) == 'submit'){
      //validasi
      $this->form_validation->set_rules('nama', 'Nama Item', 'required|min_length[4]');
      $this->form_validation->set_rules('harga', 'Harga Item', 'required|numeric');
			$this->form_validation->set_rules('stok', 'Stok Item', 'required|numeric');
      $this->form_validation->set_rules('status', 'Status', 'required|numeric');

			if ($this->form_validation->run() == TRUE){
				$name = set_value('nama');
				$items = array(
					'name_item'      => $name,
          'harga_item'     => set_value('harga'),
          'stok_item'     => set_value('stok'),
          'status_item'     => set_value('status'),
				);
				$this->session->set_flashdata('success', 'Berhasil');

				//upload gambar
				date_default_timezone_set("Asia/Kolkata");
				$time =  Date('Y-m-d h:i:s');

				$config['upload_path'] = './assets/images/';
				$config['allowed_types'] ='jpg|png|jpeg';
				$config['max_size'] ='2048';
				$config['file_name'] = 'foto'.$name;

				$this->load->library('upload', $config);

				if($this->upload->do_upload('foto')){
					$gbr = $this->upload->data();
					//proses Insert
					$items = array(
						'gambar_item' => $gbr['file_name']
					);
				}
				$this->ModelAdmin->update($kode_item, $items);
			}

		}
		$data['header'] = "Ubah Data Item";
		$this->template->admin('admin/item_form', $data);
  }

	function do_upload(){

	}

  public function delete_item($kode_item){
    $this->ModelAdmin->delete($kode_item);
    redirect('DashboardAdmin/adminHome');
  }

	function show_data($kode_item) {
		// $kode_item = $this->uri->segment(3);
    $data = $this->ModelAdmin->get_data($kode_item);
    echo json_encode($data);
	}

}
