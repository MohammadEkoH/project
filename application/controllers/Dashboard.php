<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('template');
		$this->load->library(array('template', 'form_validation'));
	}

	//-------------------User yoo-------------------//
	public function index(){
		if($this->session->userdata('email_akun')){
			redirect('dashboard/user');
		}else{
			redirect('dashboard/home');
		}
	}

	function login(){
    $email = $this->input->post('email', true);
		$password = md5($this->input->post('password', true));

		$cek = $this->Mdbase->login($email, $password);
    $result = count($cek);
    if ($result>0){
      $this->session->set_userdata('email_usr', $email);
  		redirect('dashboard/user');
    }else{
      redirect('dashboard/home');
    }
  }

	function logout(){
		$this->session->unset_userdata('email_usr');
 	 	$this->session->sess_destroy();
		redirect('dashboard/home');
	}

	function register(){
		$this->load->view('dashboard/vregister');
	}

	function add(){
		$pilih = $this->uri->segment(3);
		switch ($pilih) {
			case 'register':
				$this->form_validation->set_rules('email', 'email', 'required');
				$this->form_validation->set_rules('password', 'password', 'required');
				$this->form_validation->set_rules('name', 'name', 'required');
				$this->form_validation->set_rules('tempat', 'tempat', 'required');
				$this->form_validation->set_rules('tanggal', 'tanggal', 'required');
				$this->form_validation->set_rules('alamat', 'alamat', 'required');
				$this->form_validation->set_rules('phone', 'phone', 'required|min_lenght:11');

				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$password = md5($password);
				$name = $this->input->post('name');
				$tempat = $this->input->post('tempat');
				$tanggal = $this->input->post('tanggal');
				$jenkel = $this->input->post('jenkel');
				$alamat = $this->input->post('alamat');
				$phone = $this->input->post('phone');

				$dataUser = array(
					'name_usr' => $name,
					'email_usr' => $email,
					'tempat_usr' => $tempat,
					'tgl_usr' => $tanggal,
					'jenkel_usr' => $jenkel,
					'alamat_usr' => $alamat,
					'phone_usr' => $phone
				);

				$cek = $this->Mdbase->getDataUser($email);
		    $result = count($cek);
		    if ($result>0){
		  		redirect('dashboard/login');
		    }else{
					$this->Mdbase->insert($dataUser, $password);
		      redirect('dashboard/login');
		    }

				break;
			case 'updateDataUser':
				$email = $this->session->userdata('email_usr');

				$tempat = $this->input->post('tempat');
				$tanggal = $this->input->post('tanggal');
				$jenkel = $this->input->post('jenkel');
				$alamat = $this->input->post('alamat');
				$phone = $this->input->post('phone');

				$data = array(
					'tempat_usr' => $tempat,
					'tgl_usr' => $tanggal,
					'jenkel_usr' => $jenkel,
					'alamat_usr' => $alamat,
					'phone_usr' => $phone
				);
				$this->Mdbase->updateDataUser($email, $data);
				redirect('dashboard/settings');
				break;
			default:
				# code...
				break;
		}
	}

	function home(){
		$data['title'] = "HOME";
		$data['myData'] = $this->ModelAdmin->get_all('item')->result();

		if ($this->session->userdata('email_usr')) {
			redirect('dashboard/user');
		}else{
			$this->load->view('dashboard/vhome',$data);
		}
	}

	function user(){
		$data['title'] = "HOME";
		$email = $this->session->userdata('email_usr');
		$data['ITEM'] = $this->ModelAdmin->get_all('item')->result();
		$data['DATA'] = $this->Mdbase->getDataUser($email);

		if ($email){
			$this->load->view('user/vhome', $data);
		}else{
			redirect('dashboard/home');
		}
	}

	function itemDetail(){
		$data['title'] = "HOME | Detail";
		$kode_item = $this->uri->segment(3);
    $item = $this->ModelAdmin->get_where('item', array('kode_item' => $kode_item))->result();
		foreach ($item as $key) {
        $data['name_item'] = $key->name_item;
        $data['harga_item'] = $key->harga_item;
				$data['stok_item'] = $key->stok_item;
        $data['status_item'] = $key->status_item;
        $data['gambar_item'] = $key->gambar_item;
        $data['deskripsi_item'] = $key->deskripsi_item;
    } ;
		$rating = 10/100;
		$data['rating'] = $rating;
    $this->load->view('user/vitem_detail', $data);
	}

	function settings(){
		$data['title'] = "Setting";
		$email = $this->session->userdata('email_usr');

		if ($email) {
			$data['DATA'] = $this->Mdbase->getDataUser($email);
			$this->load->view('user/vsettings', $data);
		}
	}

	function do_upload(){
		//upload gambar
		date_default_timezone_set("Asia/Kolkata");
		$time =  Date('Y-m-d h:i:s');
		$email = $this->session->userdata('email_usr');

		$config['upload_path'] = './assets/images/';
		$config['allowed_types'] ='jpg|png|jpeg';
		$config['max_size'] ='2048';
		$config['file_name'] = 'foto'.$time;

		$this->load->library('upload', $config);

		if($this->upload->do_upload('foto')){
			$gbr = $this->upload->data();
			//proses Insert
			$data = array(
				'foto_usr' => $gbr['file_name']
			);
			$this->Mdbase->updateDataUser($email, $data);
		}
		redirect('dashboard/settings');
  }

	 function admin(){
		 $this->load->view('admin/dashboard');
	 }

}
