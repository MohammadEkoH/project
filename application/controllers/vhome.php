<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?=$title?></title>

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="../img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme-elements.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme-blog.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/rs-plugin/css/navigation.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/skins/skin-shop-5.css">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/demos/demo-shop-5.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?=base_url('')?>assets/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 155, 'stickySetTop': '-155px', 'stickyChangeLogo': false}">
				<div class="header-body">
					<div class="header-top">
						<div class="container">
							<div class="header-column welcome-msg">
								<div class="row">
									<div class="cart-area">

										<div class="nav-item header-user cart-dropdown">
											<a href="#">
												<img src="<?=base_url('assets/img/avatars/avatar-2.jpg')?>" width="30px" height="30px" alt="">
											</a>

											<div class="cart-dropdownmenu right">
												<div class="dropdownmenu-wrapper">
													<div class="cart-products">
														<div class="product product-sm align-center">
															<?php foreach ($DATA as $data): ?>
																<b><?= $data->name_usr ?></b>
															<?php endforeach; ?>
														</div>
														<div class="product product-sm align-center">
															<a href="<?=base_url('dashboard/settings')?>" class="btn">Setting</a><br>
															<a href="<?=base_url('dashboard/logout')?>" class="btn">Logout</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<a href="#" class="mmenu-toggle-btn" title="Toggle menu">
										<i class="fa fa-bars"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="demo-shop-5.html">
										<img alt="Porto" width="111" height="51" src="../img/demos/shop/logo-shop.png">
									</a>
								</div>
							</div>
							<div class="header-column">
								<div class="row">
									<div class="cart-area">
										<div class="custom-block">
											<div class="header-search">
												<a href="#" class="search-toggle"><i class="fa fa-search"></i></a>
												<form action="#" method="get">
													<div class="header-search-wrapper">
														<input type="text" class="form-control" name="cari" id="cari" placeholder="Search..." required>

														<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
													</div>
												</form>
											</div>
										</div>

										<div class="cart-dropdown">
											<a href="#" class="cart-dropdown-icon">
												<i class="minicart-icon"></i>
												<span class="cart-info">
													<span class="cart-qty">2</span>
													<span class="cart-text">item(s)</span>
												</span>
											</a>

											<div class="cart-dropdownmenu right">
												<div class="dropdownmenu-wrapper">
													<div class="cart-products">
														<div class="product product-sm">
															<a href="#" class="btn-remove" title="Remove Product">
																<i class="fa fa-times"></i>
															</a>
															<figure class="product-image-area">
																<a href="demo-shop-5-product-details.html" title="Product Name" class="product-image">
																	<img src="../img/demos/shop/products/thumbs/cart-product1.jpg" alt="Product Name">
																</a>
															</figure>
															<div class="product-details-area">
																<h2 class="product-name"><a href="demo-shop-5-product-details.html" title="Product Name">Blue Women Top</a></h2>

																<div class="cart-qty-price">
																	1 X
																	<span class="product-price">$65.00</span>
																</div>
															</div>
														</div>
														<div class="product product-sm">
															<a href="#" class="btn-remove" title="Remove Product">
																<i class="fa fa-times"></i>
															</a>
															<figure class="product-image-area">
																<a href="demo-shop-5-product-details.html" title="Product Name" class="product-image">
																	<img src="../img/demos/shop/products/thumbs/cart-product2.jpg" alt="Product Name">
																</a>
															</figure>
															<div class="product-details-area">
																<h2 class="product-name"><a href="demo-shop-5-product-details.html" title="Product Name">Black Utility Top</a></h2>

																<div class="cart-qty-price">
																	1 X
																	<span class="product-price">$39.00</span>
																</div>
															</div>
														</div>
													</div>

													<div class="cart-totals">
														Total: <span>$104.00</span>
													</div>

													<div class="cart-actions">
														<a href="demo-shop-5-cart.html" class="btn btn-primary">View Cart</a>
														<a href="demo-shop-5-checkout.html" class="btn btn-primary">Checkout</a>
													</div>
												</div>
											</div>
										</div>
									</div>
									<a href="#" class="mmenu-toggle-btn" title="Toggle menu">
										<i class="fa fa-bars"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="header-container header-nav">
						<div class="container">
							<div class="header-nav-main">
								<nav>
									<ul class="nav nav-pills" id="mainNav">
										<li class="active">
											<a href="<?=base_url('dashboard/home')?>">
												Home
											</a>
										</li>
										<li class="pull-right">
											<a href="demo-shop-5-contact-us.html">
												Contact Us <span class="tip tip-hot">Hot!</span>
											</a>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div class="mobile-nav">
				<div class="mobile-nav-wrapper">
					<ul class="mobile-side-menu">
						<li><a href="demo-shop-5.html">Home</a></li>
						<li>
							<span class="mmenu-toggle"></span>
							<a href="#">Fashion <span class="tip tip-new">New</span></a>

							<ul>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Women</a>
									<ul>
										<li>
											<a href="#">Tops &amp; Blouses</a>
										</li>
										<li>
											<a href="#">Accessories</a>
										</li>
										<li>
											<a href="#">Dresses &amp; Skirts</a>
										</li>
										<li>
											<a href="#">Shoes &amp; Boots</a>
										</li>
									</ul>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Men</a>

									<ul>
										<li>
											<a href="#">Accessories</a>
										</li>
										<li>
											<a href="#">Watch &amp; Fashion <span class="tip tip-new">New</span></a>
										</li>
										<li>
											<a href="#">Tees, Knits &amp; Polos</a>
										</li>
										<li>
											<a href="#">Pants &amp; Denim</a>
										</li>
									</ul>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Jewellery <span class="tip tip-hot">Hot</span></a>

									<ul>
										<li>
											<a href="#">Sweaters</a>
										</li>
										<li>
											<a href="#">Heels &amp; Sandals</a>
										</li>
										<li>
											<a href="#">Jeans &amp; Shorts</a>
										</li>
									</ul>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Kids Fashion</a>

									<ul>
										<li>
											<a href="#">Casual Shoes</a>
										</li>
										<li>
											<a href="#">Spring &amp; Autumn</a>
										</li>
										<li>
											<a href="#">Winter Sneakers</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<span class="mmenu-toggle"></span>
							<a href="#">Pages <span class="tip tip-hot">Hot!</span></a>

							<ul>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Category</a>
									<ul>
										<li>
											<a href="demo-shop-5-category-2col.html">2 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-3col.html">3 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-4col.html">4 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-5col.html">5 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-6col.html">6 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-7col.html">7 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-8col.html">8 Columns</a>
										</li>
										<li>
											<a href="demo-shop-5-category-right-sidebar.html">Right Sidebar</a>
										</li>
										<li>
											<a href="demo-shop-5-category-list.html">Category List</a>
										</li>
									</ul>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Category Banners</a>
									<ul>
										<li>
											<a href="demo-shop-5-category-banner-boxed-slider.html">Boxed slider</a>
										</li>
										<li>
											<a href="demo-shop-5-category-banner-boxed-image.html">Boxed Image</a>
										</li>
										<li>
											<a href="demo-shop-5-category-banner-fullwidth.html">Fullwidth</a>
										</li>
									</ul>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Product Details</a>
									<ul>
										<li>
											<a href="demo-shop-5-product-details.html">Product Details 1</a>
										</li>
										<li>
											<a href="demo-shop-5-product-details2.html">Product Details 2</a>
										</li>
										<li>
											<a href="demo-shop-5-product-details3.html">Product Details 3</a>
										</li>
										<li>
											<a href="demo-shop-5-product-details4.html">Product Details 4</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="demo-shop-5-cart.html">Shopping Cart</a>
								</li>
								<li>
									<a href="demo-shop-5-checkout.html">Checkout</a>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Loign &amp; Register</a>
									<ul>
										<li>
											<a href="demo-shop-5-login.html">Login</a>
										</li>
										<li>
											<a href="demo-shop-5-register.html">Register</a>
										</li>
									</ul>
								</li>
								<li>
									<span class="mmenu-toggle"></span>
									<a href="#">Dashboard</a>
									<ul>
										<li>
											<a href="demo-shop-5-dashboard.html">Dashboard</a>
										</li>
										<li>
											<a href="demo-shop-5-myaccount.html">My Account</a>
										</li>
									</ul>
								</li>
							</ul>
						</li>
						<li>
							<a href="demo-shop-5-about-us.html">About Us</a>
						</li>
						<li>
							<span class="mmenu-toggle"></span>
							<a href="#">Blog</a>
							<ul>
								<li><a href="demo-shop-5-blog.html">Blog</a></li>
								<li><a href="demo-shop-5-blog-post.html">Blog Post</a></li>
							</ul>
						</li>
						<li>
							<a href="demo-shop-5-contact-us.html">Contact Us</a>
						</li>
						<li>
							<a href="#">Buy Porto!</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-md-9 col-md-push-3">
						<div class="toolbar mb-none">
							<div class="sorter">
								<div class="sort-by">
									<label>Sort by:</label>
									<select>
										<option value="Position">Position</option>
										<option value="Name">Name</option>
										<option value="Price">Price</option>
									</select>
									<a href="#" title="Set Desc Direction">
										<img src="../img/demos/shop/i_asc_arrow.gif" alt="Set Desc Direction">
									</a>
								</div>

								<div class="view-mode">
									<span title="Grid">
										<i class="fa fa-th"></i>
									</span>
									<a href="#" title="List">
										<i class="fa fa-list-ul"></i>
									</a>
								</div>

								<ul class="pagination">
									<li class="active"><a href="#">1</a></li>
									<li><a href="#">2</a></li>
									<li><a href="#"><i class="fa fa-caret-right"></i></a></li>
								</ul>

								<div class="limiter">
									<label>Show:</label>
									<select>
										<option value="12">12</option>
										<option value="24">24</option>
										<option value="36">36</option>
									</select>
								</div>
							</div>
						</div>

						<ul class="products-grid columns4">
							<li>
								<div class="product">
									<figure class="product-image-area">
										<a href="demo-shop-5-product-details.html" title="<?="produk name"?>" class="product-image">
											<img src="<?=base_url('')?>assets/img/demos/shop/products/product1.jpg" alt="<?="produk name"?>">
										</a>
									</figure>
									<div class="product-details-area">
										<h2 class="product-name"><a href="demo-shop-5-product-details.html" title="<?="produk name"?>">
											Drone
										</a></h2>

										<div class="product-price-box">
											<span class="old-price">$99.00</span>
											<span class="product-price">$89.00</span>
										</div>

										<div class="product-actions">
											<a href="#" class="addtowishlist" title="Add to Wishlist">
												<i class="fa fa-heart"></i>
											</a>
										</div>
									</div>
								</div>
							</li>
						</ul>

						<div class="toolbar-bottom">
							<div class="toolbar">
								<div class="sorter">
									<ul class="pagination">
										<li class="active"><a href="#">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#"><i class="fa fa-caret-right"></i></a></li>
									</ul>

									<div class="limiter">
										<label>Show:</label>
										<select>
											<option value="12">12</option>
											<option value="24">24</option>
											<option value="36">36</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<aside class="col-md-3 col-md-pull-9 sidebar shop-sidebar">
						<div class="panel-group">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a>
											Profil
										</a>
									</h4>
								</div>
								<div>
									<div class="panel-body">
										<ul>
											<img src="" width="30" height="30" alt="">
											<?php foreach ($DATA as $data): ?>
												<a href="#" class="btn"><?=$data->name_usr?></a>
											<?php endforeach; ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</aside>
				</div>
			</div>

			</div>

			<hr class="">
			<div class="customer-support-section">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="feature-box feature-box-style-3">
								<div class="feature-box-icon">
									<i class="fa fa-star"></i>
								</div>
								<div class="feature-box-info">
									<h4>Customer Support</h4>
									<h5>YOU WON'T BE ALONE</h5>
									<p>We really care about you and your website as much as you do. Purchasing Porto or any other theme from us you get 100% free support.</p>
								</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="feature-box feature-box-style-3">
								<div class="feature-box-icon">
									<i class="fa fa-mail-reply"></i>
								</div>
								<div class="feature-box-info">
									<h4>Fully Customizable</h4>
									<h5>TONS OF OPTIONS</h5>
									<p>With Porto you can customize the layout, colors and styles within only a few minutes. Start creating an amazing website right now!</p>
								</div>
							</div>
						</div>

						<div class="col-sm-4">
							<div class="feature-box feature-box-style-3">
								<div class="feature-box-icon">
									<i class="fa fa-paper-plane"></i>
								</div>
								<div class="feature-box-info">
									<h4>Powerful Admin</h4>
									<h5>MADE TO HELP YOU</h5>
									<p>Porto has very powerful admin features to help customer to build their own shop in minutes without any special skills in web development.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>

						<div class="col-md-3">
							<h4>My Account</h4>
							<ul class="links">
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="demo-shop-5-about-us.html">About Us</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="demo-shop-5-contact-us.html">Contact Us</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="demo-shop-5-myaccount.html">My account</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="#">Orders history</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="#">Advanced search</a>
								</li>
							</ul>
						</div>
						<div class="col-md-3">
							<div class="contact-details">
								<h4>Contact Information</h4>
								<ul class="contact">
									<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong><br> 1234 Street Name, City, US</p></li>
									<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong><br> (123) 456-7890</p></li>
									<li><p><i class="fa fa-envelope-o"></i> <strong>Email:</strong><br> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
									<li><p><i class="fa fa-clock-o"></i> <strong>Working Days/Hours:</strong><br> Mon - Sun / 9:00AM - 8:00PM</p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<h4>Main Features</h4>
							<ul class="features">
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">Super Fast Template</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">1st Seller Template</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">19 Unique Shop Layouts</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">Powerful Template Features</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">Mobile &amp; Retina Optimized</a>
								</li>
							</ul>
						</div>
						<div class="col-md-3">
							<div class="newsletter">
								<h4>Be the First to Know</h4>
								<p class="newsletter-info">Get all the latest information on Events,<br> Sales and Offers. Sign up for newsletter today.</p>

								<div class="alert alert-success hidden" id="newsletterSuccess">
									<strong>Success!</strong> You've been added to our email list.
								</div>

								<div class="alert alert-danger hidden" id="newsletterError"></div>


								<p>Enter your e-mail Address:</p>
								<form id="newsletterForm" action="../php/newsletter-subscribe.php" method="POST">
									<div class="input-group">
										<input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="submit">Submit</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<p class="copyright-text">© Copyright 2017. All Rights Reserved.</p>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="<?=base_url('')?>assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/common/common.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/vide/vide.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?=base_url('')?>assets/js/theme.js"></script>


		<script src="<?=base_url('')?>assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?=base_url('')?>assets/js/views/view.contact.js"></script>

		<script src="<?=base_url('')?>assets/vendor/nouislider/nouislider.min.js"></script>

		<!-- Demo -->
		<script src="<?=base_url('')?>assets/js/demos/demo-shop-5.js"></script>

		<!-- Theme Custom -->
		<script src="<?=base_url('')?>assets/js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="<?=base_url('')?>assets/js/theme.init.js"></script>






		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
