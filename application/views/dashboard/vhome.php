<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?=$title?></title>

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="../img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="../img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/animate/animate.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/simple-line-icons/css/simple-line-icons.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/owl.carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/owl.carousel/assets/owl.theme.default.min.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/magnific-popup/magnific-popup.min.css">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme-elements.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme-blog.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/theme-shop.css">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/rs-plugin/css/settings.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/rs-plugin/css/layers.css">
		<link rel="stylesheet" href="<?=base_url('')?>assets/vendor/rs-plugin/css/navigation.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/skins/skin-shop-5.css">

		<!-- Demo CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/demos/demo-shop-5.css">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="<?=base_url('')?>assets/css/custom.css">

		<!-- Head Libs -->
		<script src="<?=base_url('')?>assets/vendor/modernizr/modernizr.min.js"></script>

	</head>
	<body>

		<div class="body">
			<header id="header" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': false, 'stickyStartAt': 155, 'stickySetTop': '-155px', 'stickyChangeLogo': false}">
				<div class="header-body">
					<div class="header-top">
						<div class="container">
							<a href="<?=base_url('dashboard/register')?>" class="welcome-msg btn btn-primary">login/register</a>
						</div>
					</div>
					<div class="header-container container">
						<div class="header-row">
							<div class="header-column">
								<div class="header-logo">
									<a href="demo-shop-5.html">
										<img alt="Porto" width="111" height="51" src="../img/demos/shop/logo-shop.png">
									</a>
								</div>
							</div>
							<div class="header-column">
								<div class="row">
									<div class="cart-area">
										<div class="custom-block">
											<div class="header-search">
												<a href="#" class="search-toggle"><i class="fa fa-search"></i></a>
												<form action="#" method="get">
													<div class="header-search-wrapper">
														<input type="text" class="form-control" name="cari" id="cari" placeholder="Search..." required>

														<button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
													</div>
												</form>
											</div>
										</div>

										<div class="cart-dropdown">
											<a href="<?=base_url('dashboard/register')?>" class="cart-dropdown-icon">
												<i class="minicart-icon"></i>
												<span class="cart-info">
													<span class="cart-qty">0</span>
													<span class="cart-text">item(s)</span>
												</span>
											</a>
										</div>
									</div>
									<a href="#" class="mmenu-toggle-btn" title="Toggle menu">
										<i class="fa fa-bars"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="header-container header-nav">
						<div class="container">
							<div class="header-nav-main">
								<nav>
									<ul class="nav nav-pills" id="mainNav">
										<li class="active">
											<a href="<?=base_url('dashboard/home')?>">
												Home
											</a>
										</li>
										<li class="pull-right">
											<a href="#">
												Contact Us <span class="tip tip-hot">Hot!</span>
											</a>
										</li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
				</div>
			</header>

			<div id="mobile-menu-overlay"></div>

			<div role="main" class="main">

			<div class="slider-container rev_slider_wrapper" style="height: 500px;">
				<div id="revolutionSlider" class="slider rev_slider manual">
					<ul>
						<li data-transition="fade">
							<img src="../img/demos/shop/slides/shop5/slide1.png"
								alt="slide bg"
								data-bgposition="center center"
								data-bgfit="cover"
								data-bgrepeat="no-repeat"
								class="rev-slidebg">

							<div class="tp-caption text-primary"
								data-x="left" data-hoffset="125"
								data-y="center" data-voffset="-60"
								data-start="500"
								data-whitespace="nowrap"
								data-transform_in="y:[100%];s:500;"
								data-transform_out="opacity:0;s:500;"
								style="z-index: 5; font-size: 60px; text-transform: uppercase; font-weight:600; line-height:1;"
								data-mask_in="x:0px;y:0px;">Huge <span style="font-weight: 800;">Sale</span></div>

							<div class="tp-caption"
								data-x="left" data-hoffset="235"
								data-y="center" data-voffset="-10"
								data-start="1000"
								style="z-index: 5; font-size: 25px; font-weight: 300; line-height:1;"
								data-transform_in="y:[100%];opacity:0;s:500;">Now starting at <span style="font-weight:400;">$99</span></div>

							<div class="tp-caption"
								data-x="left" data-hoffset="366"
								data-y="center" data-voffset="40"
								data-start="1500"
								style="z-index: 5; font-size: 16px; font-weight: 300; line-height:1;"
								data-transform_in="y:[100%];opacity:0;s:500;"><a href="#" class="btn btn-default btn-primary text-uppercase">Shop now</a></div>

							<div class="tp-caption"
								data-x="30"
								data-y="top"
								data-start="1200"
								data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;" style="z-index: 4;"><img src="../img/demos/shop/slides/shop5/item1.png" alt="Item"></div>
						</li>
						<li data-transition="fade">
							<img src="../img/demos/shop/slides/shop5/slide2.jpg"
								alt="slide bg"
								data-bgposition="center center"
								data-bgfit="cover"
								data-bgrepeat="no-repeat"
								class="rev-slidebg">

							<div class="tp-caption"
								data-x="177"
								data-y="188"
								data-start="1000"
								data-transform_in="x:[-300%];opacity:0;s:500;"><img src="../img/slides/slide-title-border.png" alt="Border"></div>

							<div class="tp-caption top-label"
								data-x="227"
								data-y="180"
								data-start="500"
								data-transform_in="y:[-300%];opacity:0;s:500;">DO YOU NEED A NEW</div>

							<div class="tp-caption"
								data-x="480"
								data-y="188"
								data-start="1000"
								data-transform_in="x:[300%];opacity:0;s:500;"><img src="../img/slides/slide-title-border.png" alt="Border"></div>

							<div class="tp-caption main-label"
								data-x="140"
								data-y="210"
								data-start="1500"
								data-whitespace="nowrap"
								data-transform_in="y:[100%];s:500;"
								data-transform_out="opacity:0;s:500;"
								data-mask_in="x:0px;y:0px;">eCOMMERCE?</div>

							<div class="tp-caption bottom-label"
								data-x="185"
								data-y="280"
								data-start="2000"
								data-transform_in="y:[100%];opacity:0;s:500;">Check out our options and features.</div>
						</li>
						<li data-transition="fade">
							<img src="../img/demos/shop/slides/shop5/slide3.png"
								alt="slide bg"
								data-bgposition="center center"
								data-bgfit="cover"
								data-bgrepeat="no-repeat"
								class="rev-slidebg">
						</li>
					</ul>
				</div>
			</div>

			<div class="homepage-bar">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<i class="fa fa-truck bar-icon"></i>
							<div class="bar-textarea">
								<h3>FREE SHIPPING &amp; RETURN</h3>
								<p>Free shipping on all orders over $99.</p>
							</div>
						</div>
						<div class="col-md-4">
							<i class="fa fa-dollar bar-icon"></i>
							<div class="bar-textarea">
								<h3>MONEY BACK GUARANTEE</h3>
								<p>100% money back guarantee.</p>
							</div>
						</div>
						<div class="col-md-4">
							<i class="fa fa-support bar-icon"></i>
							<div class="bar-textarea">
								<h3>ONLINE SUPPORT 24/7</h3>
								<p>Lorem ipsum dolor sit amet.</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<hr>

			<div class="container mb-xs">
				<h2 class="slider-title">
                    <span class="inline-title">New PRODUCTS</span>
                    <span class="line"></span>
                </h2>

                <div class="owl-carousel owl-theme manual new-products-carousel hide-addtolinks">

				  <?php foreach ($myData as $item): ?>
						<div class="product">
							<figure class="product-image-area">
								<a href="<?=base_url('dashboard/register')?>" title="<?=$item->name_item?>" class="product-image">
									<img src="<?=base_url('')?>./assets/images/<?=$item->gambar_item?>" alt="<?=$item->name_item?>">
								</a>
								<div class="product-label"><span class="new">New</span></div>
							</figure>
							<div class="product-details-area">
								<h2 class="product-name"><a href="<?=base_url('dashboard/itemDetail/'.$item->kode_item)?>" title="<?=$item->name_item?>">
									<?php
									$long_string = $item->name_item;
									$limited_string = substr($long_string, 0, 14);
									echo $limited_string;
									?>
								</a></h2>
								<div class="product-ratings">
									<div class="ratings-box">
										<div class="rating" style="width:80%"></div>
									</div>
								</div>

								<div class="product-price-box">
									<span class="product-price"><?= 'Rp.' .number_format($item->harga_item,0,',','.'); ?></span>
								</div>

								<div class="product-actions">
									<a href="#" class="addtowishlist" title="Add to Wishlist">
										<i class="fa fa-heart"></i>
									</a>
									<a href="#" class="comparelink" title="Add to Compare">
										<i class="glyphicon glyphicon-signal"></i>
									</a>
								</div>
							</div>
						</div>
				  <?php endforeach; ?>
				</div>
			</div>
			<hr>
			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="footer-ribbon">
							<span>Get in Touch</span>
						</div>

						<div class="col-md-3">
							<h4>My Account</h4>
							<ul class="links">
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="demo-shop-5-about-us.html">About Us</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="demo-shop-5-contact-us.html">Contact Us</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="demo-shop-5-myaccount.html">My account</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="#">Orders history</a>
								</li>
								<li>
									<i class="fa fa-caret-right text-color-primary"></i>
									<a href="#">Advanced search</a>
								</li>
							</ul>
						</div>
						<div class="col-md-3">
							<div class="contact-details">
								<h4>Contact Information</h4>
								<ul class="contact">
									<li><p><i class="fa fa-map-marker"></i> <strong>Address:</strong><br> 1234 Street Name, City, US</p></li>
									<li><p><i class="fa fa-phone"></i> <strong>Phone:</strong><br> (123) 456-7890</p></li>
									<li><p><i class="fa fa-envelope-o"></i> <strong>Email:</strong><br> <a href="mailto:mail@example.com">mail@example.com</a></p></li>
									<li><p><i class="fa fa-clock-o"></i> <strong>Working Days/Hours:</strong><br> Mon - Sun / 9:00AM - 8:00PM</p></li>
								</ul>
							</div>
						</div>
						<div class="col-md-3">
							<h4>Main Features</h4>
							<ul class="features">
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">Super Fast Template</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">1st Seller Template</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">19 Unique Shop Layouts</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">Powerful Template Features</a>
								</li>
								<li>
									<i class="fa fa-check text-color-primary"></i>
									<a href="#">Mobile &amp; Retina Optimized</a>
								</li>
							</ul>
						</div>
						<div class="col-md-3">
							<div class="newsletter">
								<h4>Be the First to Know</h4>
								<p class="newsletter-info">Get all the latest information on Events,<br> Sales and Offers. Sign up for newsletter today.</p>

								<div class="alert alert-success hidden" id="newsletterSuccess">
									<strong>Success!</strong> You've been added to our email list.
								</div>

								<div class="alert alert-danger hidden" id="newsletterError"></div>


								<p>Enter your e-mail Address:</p>
								<form id="newsletterForm" action="../php/newsletter-subscribe.php" method="POST">
									<div class="input-group">
										<input class="form-control" placeholder="Email Address" name="newsletterEmail" id="newsletterEmail" type="text">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="submit">Submit</button>
										</span>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="footer-copyright">
					<div class="container">
						<a href="index.html" class="logo">
							<img alt="Porto Website Template" class="img-responsive" src="../img/demos/shop/logo-footer.png">
						</a>
						<ul class="social-icons">
							<li class="social-icons-facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook"><i class="fa fa-facebook"></i></a></li>
							<li class="social-icons-twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter"><i class="fa fa-twitter"></i></a></li>
							<li class="social-icons-linkedin"><a href="http://www.linkedin.com/" target="_blank" title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
						</ul>
						<img alt="Payments" src="../img/demos/shop/payments.png" class="footer-payment">
						<p class="copyright-text">© Copyright 2017. All Rights Reserved.</p>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="<?=base_url('')?>assets/vendor/jquery/jquery.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.appear/jquery.appear.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery-cookie/jquery-cookie.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/common/common.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.validation/jquery.validation.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.gmap/jquery.gmap.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/jquery.lazyload/jquery.lazyload.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/isotope/jquery.isotope.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/vide/vide.min.js"></script>

		<!-- Theme Base, Components and Settings -->
		<script src="<?=base_url('')?>assets/js/theme.js"></script>


		<script src="<?=base_url('')?>assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
		<script src="<?=base_url('')?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

		<!-- Current Page Vendor and Views -->
		<script src="<?=base_url('')?>assets/js/views/view.contact.js"></script>



		<!-- Demo -->
		<script src="<?=base_url('')?>assets/js/demos/demo-shop-5.js"></script>

		<!-- Theme Custom -->
		<script src="<?=base_url('')?>assets/js/custom.js"></script>

		<!-- Theme Initialization Files -->
		<script src="<?=base_url('')?>assets/js/theme.init.js"></script>






		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->


	</body>
</html>
