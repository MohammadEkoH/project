
$(document).ready(function() {
    var kode_item = $('#idKodeItem').val();
    $.ajax({
        url : "<?php echo site_url('DashboardAdmin/show_data')?>" + kode_item,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
            $('#name').text(data.name_item);
            $('#harga').text(data.harga_item);
        }
    });
})
