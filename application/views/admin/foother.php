<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url();?>admin_assets/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url();?>admin_assets/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url();?>admin_assets/js/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url();?>admin_assets/js/adminlte.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo base_url();?>admin_assets/js/jquery.sparkline.min.js"></script>
<!-- jvectormap  -->
<script src="<?php echo base_url();?>admin_assets/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url();?>admin_assets/js/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url();?>admin_assets/js/jquery.slimscroll.min.js"></script>

</body>
</html>