<div class="x-panel">
  <div class="x_title">
    <h2>Managemen Item</h2>
    <div style="float: right">
      <a data-toggle="modal" data-target="#addDataItem" class="btn btn-primary" >Tambah Item</a>
      <!-- Modal  -->
      <div class="modal fade" id="addDataItem" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="formModalLabel">Tambahkan Item Baru</h4>
            </div>
            <div class="modal-body">
              <form class="form-horizontal form-label-left" action="<?=base_url('DashboardAdmin/add_item')?>" enctype="multipart/form-data" method="post">
                  <input type="text" name="kode" hidden value="">
                   <div class="form-group">
                     <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nama Item
                     </label>
                     <div class="col-md-7 col-sm-6 col-xs-12">
                       <input type="text" class="form-control col-md-7 col-xs-12" name="nama" placeholder="Contoh : Item">
                     </div>
                   </div>

                   <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Item</label>
                       <div class="col-md-7 col-sm-6 col-xs-12">
                         <input class="form-control col-md-7 col-xs-12" type="number" name="harga" placeholder="Contoh : 1000">
                       </div>
                   </div>

                   <div class="form-group">
                       <label class="control-label col-md-3 col-sm-3 col-xs-12" >Stok Item</label>
                       <div class="col-md-7 col-sm-6">
                           <input class="form-control col-md-7" type="number" placeholder="Contoh : 1" name="stok" >
                        </div>
                   </div>

                   <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                        <div class="col-md-7 col-sm-6">
                            <select name="status" class="form-control">
                                <option value="">--Pilih Status--</option>
                                <option value="1"<?php if('$status == 1') {echo "selected";} ?>>Aktif</option>
                                <option value="2"<?php if('$status == 2') {echo "selected";} ?>>Tidak Aktif</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
                        <div class="col-md-7 col-sm-4">
                              <textarea class="form-control" rows="4" name="desk" placeholder="Contoh : Item ini asli produk Indonesia.."></textarea>
                        </div>
                    </div>

                     <div class="form-group">
                         <label class="control-label col-md-3 col-sm-3 col-xs-12" >Gambar Item
                         </label>
                         <div class="col-md-7 col-sm-6 col-xs-12">
                           <input type="file" class="form-control col-md-5 col-xs-12" name="foto" required>
                         </div>
                     </div>

                    <div class="form-group">
                      <div class="col-md-7 col-sm-6 col-sx-12 col-md-offset-3">
                          <button type="submit" class="btn btn-success" name="submit" value="submit">Submit</button>
                          <button type="reset" class="btn btn-primary">Cancel</button>
                      </div>
                    </div>
            </form>
            </div>
          </div>
        </div>
    </div>
    <div class="clearfix"></div>
  </div>

  <div class="x-content">
      <table class="table table-striped table-bordered dt-responsive nowrap" id="datatable">
          <thead>
              <tr>
                  <th>Kode Item</th>
                  <th>Nama Item</th>
                  <th>Harga</th>
                  <th>Status</th>
                  <th>Opsi</th>
              </tr>
          </thead>
      <tbody>
        <?php
          foreach ($data->result() as $items) :
         ?>
         <tr>
             <td><?= $items->kode_item?></td>
             <td><?= $items->name_item; ?></td>
             <td><?= 'Rp.' .number_format($items->harga_item,0,',','.'); ?></td>
             <td>
                <?php
                 if ($items->status_item == 1) {
                   echo '<label class="label-success" style="color:white; padding:3px 5px;">Aktif</label>';
                 } else {
                   echo '<label class="label-danger" style="color:white; padding:3px 5px;">Aktif</label>';
                 }
                ?>
             </td>
             <td>
               <a href="" data-toggle="modal" data-target="#showDataItem" class="btn btn-success"><i class="fa fa-search-plus"></i></a>
               <a href="<?=base_url('DashboardAdmin/edit_item/' . $items->kode_item)?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
               <a href="<?=base_url('DashboardAdmin/delete_item/' . $items->kode_item)?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
             </td>
         </tr>
       <?php endforeach; ?>
       </div>
      </tbody>
    </table>
    <!-- Modal Yoo -->
    <div class="modal fade" id="showDataItem" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="formModalLabel">Lihat Detail Item</h4>
          </div>
          <div class="modal-body">
            <div class="col-md-4 align-center">
              <img src="<?=base_url('')?>assets/images/" width="200px" height="250px" alt="<?=$gambar_item?>">
            </div>
            <div id="idKodeItem"  class="col-md-8">
                <div class="">
                  <span id="nama"></span>
                </div><br>
                <div class="">
                  <span id="harga"></span>
                </div><br>
                <div class="">
                  <span id="stok"></span>
                </div><br>
                <div class="">
                  <span id="status"></span>
                </div><br>
                <div class="">
                  <span id="deskripsi"></span>
                </div><br>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Modal Yoo -->
  </div>
</div>
