<div class="x_panel">
     <div class="x_title">
        <div class="clearfix"></div>
        <?= validation_errors('<p style="color:blue">','</p>'); ?>
        <?php
          if($this->session->flashdata('alert'))
          {
            echo '<div class="alert alert-danger alert-message">';
            echo $this->session->flashdata('alert');
            echo '</div>';
          }
          if($this->session->flashdata('success'))
          {
            echo '<div class="alert alert-success alert-message">';
            echo $this->session->flashdata('success');
            echo '</div>';
          }
        ?>

        <script>
          var timeout = 3000; // in miliseconds (3*1000)
          $('.alert').delay(timeout).fadeOut(300);
        </script>
      </div>
      <div class="x_content">
          <span> <h2><?=$header?></h2> </span>
          <br />
          <form class="form-horizontal form-label-left" action="" enctype="multipart/form-data" method="post">
            <div class="form-group">
              <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Nama Item
                </label>
                <div class="col-md-7 col-sm-6 col-xs-12">
                  <input type="text" class="form-control col-md-7 col-xs-12" name="nama" value="<?=$name_item?>">
                </div>
              </div>

              <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga Item</label>
                  <div class="col-md-7 col-sm-6 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" type="number" name="harga" value="<?=$harga_item?>">
                  </div>
              </div>

              <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" >Stok Item</label>
                  <div class="col-md-7 col-sm-6">
                      <input class="form-control col-md-7" type="number" value="<?=$stok_item?>" name="stok">
                   </div>
              </div>

              <div class="form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Status</label>
                   <div class="col-md-7 col-sm-6">
                       <select name="status" class="form-control">
                           <option value="<?=$status_item?>">--Pilih Status--</option>
                           <option value="1"<?php if('$status == 1') {echo "selected";} ?>>Aktif</option>
                           <option value="2"<?php if('$status == 2') {echo "selected";} ?>>Tidak Aktif</option>
                       </select>
                   </div>
               </div>

               <div class="form-group">
                   <label class="control-label col-md-3 col-sm-3 col-xs-12">Deskripsi</label>
                   <div class="col-md-7 col-sm-4">
                         <textarea class="form-control" rows="4" name="desk" value=""></textarea>
                   </div>
               </div>
            </div>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" >Gambar Item
                </label>
                <div class="col-md-7 col-sm-4">
                  <img src="<?=base_url('./assets/images/'.$gambar_item)?>" width="160" height="150px" alt="<?=$gambar_item?>">
                  <input type="file" class="form-control" name="foto" value="<?=$gambar_item?>">
                </div>
            </div>
            <div class="form-group">
              <div class="col-md-7 col-sm-6 col-sx-12 col-md-offset-3">
                  <button type="submit" class="btn btn-success" name="submit" value="submit">Simpan</button>
              </div>
            </div>
        </form>
    </div>
</div>
<hr>
